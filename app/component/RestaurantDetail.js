import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';
class RestaurantDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      routeID: this.props.route.params.id,
    };
  }

  async componentDidMount() {
    try {
      const restaurantApi = await fetch(
        `https://prueba-lab8.herokuapp.com/api/restaurante_id/${this.state.routeID}`,
      );
      const restaurant = await restaurantApi.json();
      this.setState({items: restaurant});
      console.log('DETALLES', this.state.items);
    } catch (err) {
      console.log('Error fetching data-----------', err);
    }
  }

  render() {
    const {name, description, imagePath} = this.state.items;
    return (
      <ScrollView style={styles.container}>
        <View>
          <Image
            source={{
              uri: `https://prueba-lab8.herokuapp.com/public/images/${imagePath}`,
            }}
            style={styles.backgroundImage}
            resizeMode="cover"
          />

          <Text style={styles.title}>{name}</Text>
          <Text style={styles.description}>{description}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('Third', {
              id: this.state.routeID,
              latitude: this.state.items.latitude,
              latitude: this.state.items.longitude,
            });
          }}
          style={{backgroundColor: 'white', borderRadius: 10, marginTop: 20}}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              alignSelf: 'center',
              padding: 10,
            }}>
            Ubicar
          </Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: '#202020',
  },
  backgroundImage: {
    width: responsiveWidth(100),
    height: responsiveHeight(25),
  },
  image: {
    width: responsiveWidth(50),
    height: responsiveHeight(30),
    position: 'absolute',
    left: 20,
    top: 40,
  },
  option: {
    flexDirection: 'row-reverse',
    paddingTop: 20,
    paddingLeft: 20,
  },
  iconOption: {
    marginLeft: 15,
  },
  title: {
    color: 'white',
    fontSize: 20,
    paddingTop: 35,
    fontWeight: 'bold',
  },
  description: {
    fontSize: 15,
    color: 'white',
    paddingTop: 15,
  },
  genres: {
    fontSize: 15,
    color: 'white',
    paddingTop: 15,
  },
});

export default RestaurantDetail;
