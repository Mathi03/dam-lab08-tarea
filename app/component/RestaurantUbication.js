import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';
import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';

const {width, height} = Dimensions.get('window');

const SCREEN_HEIGHT = height;
const SCREEN_WIDTH = width;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class RestaurantUbication extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      routeID: this.props.route.params.id,
      estado: false,
    };
  }

  async componentDidMount() {
    try {
      const restaurantApi = await fetch(
        `https://prueba-lab8.herokuapp.com/api/restaurante_id/${this.state.routeID}`,
      );
      const restaurant = await restaurantApi.json();
      this.setState({items: restaurant});
      this.setState({estado: true});
      console.log('UBICATION', this.state.items);
    } catch (err) {
      console.log('Error fetching data-----------', err);
    }
  }

  render() {
    const {name, latitude, longitude, imagePath} = this.state.items;
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          initialRegion={
            this.state.estado
              ? {
                  latitude: latitude,
                  longitude: longitude,
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LONGITUDE_DELTA,
                }
              : {
                  latitude: -16.409046,
                  longitude: -71.537453,
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LONGITUDE_DELTA,
                }
          }>
          <Marker
            coordinate={
              this.state.estado
                ? {
                    latitude: latitude,
                    longitude: longitude,
                  }
                : {
                    latitude: -16.709046,
                    longitude: -71.537453,
                  }
            }>
            <Callout>
              <View
                style={{
                  backgroundColor: '#FFFFFF',
                  borderRadius: 5,
                }}>
                <Image
                  source={{
                    uri: `https://prueba-lab8.herokuapp.com/public/images/${imagePath}`,
                  }}
                  style={{height: 100, width: 100}}></Image>
                <Text>{name}</Text>
              </View>
            </Callout>
            {/* <View style={styles.radius}>
              <View style={styles.marker}> </View>
            </View> */}
          </Marker>
        </MapView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  radius: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    overflow: 'hidden',
    backgroundColor: 'rgba(0,122,255,0.1)',
    borderWidth: 1,
    borderColor: 'rgba(0,122,255,0.3)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  marker: {
    height: 20,
    width: 20,
    borderWidth: 3,
    borderColor: 'white',
    borderRadius: 20 / 2,
    overflow: 'hidden',
    backgroundColor: '#007AFF',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
  },
});
