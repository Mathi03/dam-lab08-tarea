import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';
import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';

export default class Maps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      places: [
        {
          name: 'Tecsup Norte',
          coordinates: {
            latitude: -8.1494049,
            longitude: -79.0437666,
          },
        },
        {
          name: 'Tecsup Centro',
          coordinates: {
            latitude: -12.0447884,
            longitude: -76.9545117,
          },
        },
        {
          name: 'Tecsup Sur',
          coordinates: {
            latitude: -16.4288528,
            longitude: -71.5038502,
          },
        },
      ],
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          initialRegion={{
            latitude: -16.409046,
            longitude: -71.537453,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}>
          {this.state.places.map((item, index) => {
            return (
              <Marker
                key={index}
                coordinate={{
                  latitude: item.coordinates.latitude,
                  longitude: item.coordinates.longitude,
                }}>
                <Callout>
                  <View
                    style={{
                      backgroundColor: '#FFFFFF',
                      borderRadius: 5,
                    }}>
                    <Text>{item.name}</Text>
                  </View>
                </Callout>
              </Marker>
            );
          })}
        </MapView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    height: responsiveHeight(100),
    width: responsiveWidth(95),
  },
});
