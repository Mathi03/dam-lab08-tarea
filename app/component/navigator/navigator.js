import React, {Component} from 'react';
import {View, Text, Button, Image} from 'react-native';

import {NavigationContainer, TabActions} from '@react-navigation/native';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import TransferenceFirst from '../TransferenceFirst';
import TransferenceSecond from '../TransferenceSecond';
import TransferenceThird from '../TransferenceThird';
import MapsScreen from '../maps/Maps';
import Restaurant from '../Restaurant';
import RestaurantDetail from '../RestaurantDetail';
import RestaurantUbication from '../RestaurantUbication';

const TransferenceStack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

function LogoTitle() {
  return (
    <Image
      style={{width: 50, height: 50, tintColor: 'white'}}
      source={require('../bank.png')}
    />
  );
}

function TransferenceStackScreen() {
  return (
    <TransferenceStack.Navigator
      screenOptions={{
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <TransferenceStack.Screen
        name="First"
        component={TransferenceFirst}
        options={({navigation, route}) => ({
          title: 'Tranferencia',
          headerLeft: (props) => <LogoTitle />,
          headerTintColor: 'white',
          headerStyle: {backgroundColor: 'black'},
        })}
      />
      <TransferenceStack.Screen
        name="Second"
        component={TransferenceSecond}
        options={({navigation, route}) => ({
          title: 'Confirmar Datos',
          headerLeft: (props) => <LogoTitle />,
          headerTintColor: 'white',
          headerStyle: {backgroundColor: 'black'},
        })}
      />
      <TransferenceStack.Screen
        name="Third"
        component={TransferenceThird}
        options={({navigation, route}) => ({
          title: 'Traferencia Exitosa',
          headerLeft: (props) => <LogoTitle />,
          headerTintColor: 'white',
          headerStyle: {backgroundColor: 'black'},
        })}
      />
    </TransferenceStack.Navigator>
  );
}
function HomeScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() => navigation.navigate('Details')}
      />
      <Button
        title="Replace Details"
        onPress={() => navigation.replace('Details')}
      />
    </View>
  );
}

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <NavigationContainer>
        <Tab.Navigator
          initialRouteName="Nuevo"
          tabBarOptions={{
            activeTintColor: '#e91e63',
          }}
          barStyle={{
            backgroundColor: 'black',
          }}>
          <Tab.Screen
            name="Home"
            component={HomeScreen}
            options={{
              tabBarLabel: 'Inicio',
              tabBarIcon: ({color}) => (
                <MaterialCommunityIcons name="home" color={color} size={25} />
              ),
            }}
          />

          <Tab.Screen
            name="Transference"
            component={TransferenceStackScreen}
            options={{
              tabBarLabel: 'Transference',
              tabBarIcon: ({color, size}) => (
                <MaterialCommunityIcons name="home" color={color} size={25} />
              ),
            }}
          />
          <Tab.Screen
            name="Nuevo"
            component={RestaurantStackScreen}
            options={{
              tabBarLabel: 'Nuevo',
              tabBarIcon: ({color, size}) => (
                <MaterialCommunityIcons name="bell" color={color} size={25} />
              ),
            }}
          />
          <Tab.Screen
            name="Maps"
            component={MapsScreen}
            options={{
              tabBarLabel: 'GOOGLE Maps',
              tabBarIcon: ({color, size}) => (
                <MaterialCommunityIcons
                  name="google-maps"
                  color={color}
                  size={25}
                />
              ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
}
const RestaurantStack = createStackNavigator();
function RestaurantStackScreen() {
  return (
    <RestaurantStack.Navigator
      screenOptions={{
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <RestaurantStack.Screen
        name="First"
        component={Restaurant}
        options={({navigation, route}) => ({
          title: 'Restaurantes',

          headerTintColor: 'white',
          headerStyle: {backgroundColor: 'black'},
        })}
      />
      <RestaurantStack.Screen
        name="Second"
        component={RestaurantDetail}
        options={({navigation, route}) => ({
          title: 'Detalle del Restaurante',

          headerTintColor: 'white',
          headerStyle: {backgroundColor: 'black'},
        })}
      />
      <RestaurantStack.Screen
        name="Third"
        component={RestaurantUbication}
        options={({navigation, route}) => ({
          title: 'Ubicacion',

          headerTintColor: 'white',
          headerStyle: {backgroundColor: 'black'},
        })}
      />
    </RestaurantStack.Navigator>
  );
}
