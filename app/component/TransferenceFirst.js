import React, {useState} from 'react';
import {
  View,
  Text,
  Button,
  TextInput,
  StyleSheet,
  Switch,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';

import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import ModalSelector from 'react-native-modal-selector';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {SafeAreaView} from 'react-native-safe-area-view';

export default function TransferenceFirst({navigation}) {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isBodyText, setBodyText] = useState('     /     /       ');
  const [isAccept, setAccept] = useState(false);
  const [istextInputValue, settextInputValue] = useState(''); //origin
  const [istextInputValue2, settextInputValue2] = useState(''); //destine
  const [isAmount, setAmount] = useState(''); //amount
  const [isReference, setReference] = useState(''); //reference
  const [isChange, setChange] = useState(false);

  let datos = {
    origin: istextInputValue,
    destine: istextInputValue2,
    amount: isAmount,
    reference: isReference,
    date: isBodyText,
    mail: isAccept,
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    //console.warn('A date has been picked: ', date);
    setChange(true);
    hideDatePicker();
    setBodyText(`${moment(date).format('DD/MM/YYYY')}`);
  };
  //let textInputValue = '';

  let index = 0;
  const data = [
    {key: index++, section: true, label: 'Cuentas'},
    {key: index++, label: '00000123456789'},
    {key: index++, label: '00000987654321'},
  ];
  const data2 = [
    {key: index++, section: true, label: 'Cuentas'},
    {key: index++, label: '00000123456789'},
    {key: index++, label: '00000987654321'},
  ];

  function nextView() {
    if (istextInputValue == '') {
      Alert.alert('Importante', 'Ingresar la cuenta origen');
    } else if (istextInputValue2 == '') {
      Alert.alert('Importante', 'Ingresar la cuenta destino');
    } else if (isAmount == '') {
      Alert.alert('Importante', 'Ingresar el importe');
    } else if (isReference == '') {
      Alert.alert('Importante', 'Ingresar referencia');
    } else if (!isChange) {
      Alert.alert('Importante', 'Debe de seleccionar un fecha');
    } else {
      navigation.navigate('Second', {
        datos,
      });
    }
  }

  return (
    <ScrollView style={styles.container}>
      <View style={{marginBottom: 30}}>
        <Text style={styles.text}>Cuenta origen</Text>
        <ModalSelector
          data={data}
          initValue="Cuenta de origen"
          supportedOrientations={['landscape']}
          accessible={true}
          scrollViewAccessibilityLabel={'Scrollable options'}
          cancelButtonAccessibilityLabel={'Cancel Button'}
          onChange={(option) => {
            settextInputValue(option.label);
          }}>
          <TextInput
            style={styles.TextInput}
            editable={false}
            placeholder="Cuenta de origen"
            value={istextInputValue}
          />
        </ModalSelector>
        <Text style={styles.text}>Cuenta destino</Text>
        <ModalSelector
          data={data2}
          initValue="Cuenta destino"
          supportedOrientations={['landscape']}
          accessible={true}
          scrollViewAccessibilityLabel={'Scrollable options'}
          cancelButtonAccessibilityLabel={'Cancel Button'}
          onChange={(option) => {
            settextInputValue2(option.label);
          }}>
          <TextInput
            style={styles.TextInput}
            editable={false}
            placeholder="Cuenta destino"
            value={istextInputValue2}
          />
        </ModalSelector>
        <Text style={styles.text}>Importe</Text>
        <TextInput
          placeholder="Importe"
          style={styles.TextInput}
          keyboardType={'numeric'}
          value={isAmount}
          onChangeText={(text) => {
            if (/^\d+$/.test(text) || text === '') {
              setAmount(text);
            }
          }}
        />
        <Text style={styles.text}>Referencia</Text>

        <TextInput
          placeholder="Referencia"
          style={styles.TextInput}
          onChangeText={(text) => setReference(text)}
          value={isReference}
        />

        <TouchableOpacity onPress={showDatePicker} style={styles.dateTime}>
          <Text>{isBodyText}</Text>
        </TouchableOpacity>

        <DateTimePicker
          //date={new Date(moment(new Date()).format('DD/MM/YYYY'))}
          mode="date"
          isVisible={isDatePickerVisible}
          onConfirm={handleConfirm}
          onCancel={hideDatePicker}
          isDarkModeEnabled={true}
        />
        <View style={styles.switch}>
          <Text>Notificarme al email</Text>
          <Switch
            onValueChange={() => {
              setAccept((previousState) => !previousState);
              //console.log(isAccept);
            }}
            value={isAccept}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={nextView}>
          <Text style={styles.textButton}>SIGUIENTE</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    padding: 20,
  },
  text: {
    fontSize: 15,
    marginTop: 20,
  },
  TextInput: {
    backgroundColor: '#e2e2e2',
    borderRadius: 5,
    textAlign: 'center',
  },
  dateTime: {
    backgroundColor: '#e2e2e2',
    alignSelf: 'center',
    padding: 10,
    borderRadius: 5,
    marginTop: 20,
  },
  switch: {
    marginTop: 20,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    paddingLeft: 30,
    paddingRight: 30,
    padding: 5,
    marginTop: 20,
    alignSelf: 'center',
    backgroundColor: '#1e12a1',

    borderRadius: 5,
  },
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
});
