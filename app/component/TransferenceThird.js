import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default class TransferenceSecond extends Component {
  constructor(props) {
    super(props);
    this.state = {
      importe: 0,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <AntDesign name="checkcircle" color={'#1bda1e'} size={200}></AntDesign>
        <TouchableOpacity
          style={[styles.button, {backgroundColor: '#1e12a1'}]}
          onPress={() => this.props.navigation.navigate('First')}>
          <Text style={[styles.textButton, {color: 'white'}]}>ACEPTAR</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  button: {
    paddingLeft: 30,
    paddingRight: 30,
    padding: 5,
    borderRadius: 5,
    marginTop: 20,
  },
  textButton: {
    fontWeight: 'bold',
    fontSize: 20,
  },
});
